
import { Container, Row } from 'react-bootstrap';
import { Route, BrowserRouter, Switch } from 'react-router-dom';

import Header from './components/header';
import Home from './pages/home.page';
import User from './pages/user.page';

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';


function App() {
  return (
    <Container fluid>
      <Row>
        <BrowserRouter>
          <Header />
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route path="/user">
              <User />
            </Route>
          </Switch>
        </BrowserRouter>
      </Row>
    </Container>
  );
}

export default App;
