import React, { Component, Fragment } from "react";
import { Row, Col, Form, Button, Container, Table } from "react-bootstrap";

class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            firstname: "",
            lastname: "",
            username:"",
            data: [
                {
                    firstname: "Jacob",
                    lastname: "Thornton",
                    username: "fat"
                },
                {
                    firstname: "Larry",
                    lastname: "Syamsudin",
                    username: "tresoon"
                }
            ]
        }
        this.handleChange = this.handleChange.bind(this);
        this.submitData = this.submitData.bind(this);
    }

    handleChange(key, value) {
        this.setState({
            [key]:value
        })
    }

    submitData(event) {
        event.preventDefault()
        const { data, firstname, lastname, username } = this.state
        data.push({ firstname, lastname, username })
        this.setState({data})
    }
    render() {
        const { data, firstname, lastname, username} = this.state
        return (
            <Fragment>
                <Container>
                    <Row>
                        <Col>
                            <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Username</th>
                                </tr>
                                </thead>
                                <tbody>
                                 {
                                    data.map((item, index) => {
                                         return (
                                <tr key={index}> 
                                    <td>{index + 1}</td>
                                    <td>{item.firstname}</td>
                                    <td>{item.lastname}</td>
                                    <td>@{item.username}</td>
                                </tr>
                                        )
                                    })
                                 }
                                 </tbody>
                           </Table>
                        </Col>
                        <Col>
                            <Form onSubmit={this.submitData}>
                                <Form.Group className="mb-3" controlId="firstname">
                                    <Form.Label>First Name</Form.Label>
                                    <Form.Control 
                                        type="text"
                                        placeholder="Input your first name"
                                        value={firstname}
                                        onChange={(event)=> this.handleChange('firstname',event.target.value)}
                                    />
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="lastname">
                                    <Form.Label>Last Name</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Input your last name"
                                        value={lastname}
                                        onChange={(event) => this.handleChange('lastname', event.target.value)}
                                    />
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="username">
                                    <Form.Label> username</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Input your username"
                                        value={username}
                                        onChange={(event) => this.handleChange('username', event.target.value)}
                                    />
                                </Form.Group>
                                <Button variant="primary" type="submit">
                                    Submit
                                </Button>
                            </Form>
                        </Col>
                    </Row>
                </Container>
            </Fragment>
        )
    }
}

export default Home